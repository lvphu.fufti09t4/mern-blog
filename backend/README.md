# NodeJS

- Add .env file to run
- npm run dev (development)

- Check error eslint: npm run lint
- Auto fix error eslint: npm run lint:fix
- Check error prettier: npm run prettier
- Auto fix error prettier: npm run prettier:fix
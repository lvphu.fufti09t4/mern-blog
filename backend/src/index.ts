import express from "express";
import http from "http";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import compression from "compression";
import cors from "cors";
import mongoose from "mongoose";
import router from "./router";
import dotenv from "dotenv";

const app = express();

app.use(
  cors({
    credentials: true,
  })
);

app.use(compression());
app.use(cookieParser());
app.use(bodyParser.json());

const server = http.createServer(app);

// For env File
dotenv.config();
const port = process.env.PORT;
const mongoUrl = process.env.MONGO_URL ?? "";

server.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});

mongoose.Promise = Promise;
mongoose
  .connect(mongoUrl)
  .then(() => {
    console.log("MongoDb is connected!");
  })
  .catch((err) => console.log(err));

app.use("/", router());

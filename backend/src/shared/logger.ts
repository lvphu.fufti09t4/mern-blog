import { createLogger, format, transports } from "winston";

const customFormat = format.printf(({ level, message, timestamp, ...metadata }) => {
  return `[${timestamp}] [${level}]: ${message} ${Object.keys(metadata).length ? JSON.stringify(metadata) : ""}`;
});

const logger = createLogger({
  format: format.combine(format.timestamp(), format.json(), customFormat),
  transports: [new transports.Console(), new transports.File({ filename: "logs/app.log", format: customFormat })],
});

export default logger;

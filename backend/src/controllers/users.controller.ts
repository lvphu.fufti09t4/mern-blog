import express from "express";
import { deleteUserById, getUserById, getUsers } from "../db/users";
import logger from "shared/logger";

export const getAllUsers = async (req: express.Request, res: express.Response) => {
  try {
    logger.info("[users.controller][getAllUsers] start");
    const users = await getUsers();
    logger.error("[users.controller][getAllUsers] errors", users);
    return res.status(200).json(users);
  } catch (error) {
    console.log(error);
    logger.error("[users.controller][getAllUsers] error", error);
    return res.sendStatus(400);
  }
};

export const deleteUser = async (req: express.Request, res: express.Response) => {
  try {
    const { id } = req.params;

    const deletedUser = await deleteUserById(id);

    return res.json(deletedUser);
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
};

export const updateUser = async (req: express.Request, res: express.Response) => {
  try {
    const { id } = req.params;
    const { username } = req.body;

    if (!username) {
      return res.sendStatus(400);
    }

    const user = await getUserById(id);

    if (!user) {
      return res.sendStatus(400);
    }

    user.username = username;
    await user.save();

    return res.status(200).json(user).end();
  } catch (error) {
    console.log(error);
    return res.sendStatus(400);
  }
};
